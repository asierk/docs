---
title: "Important notice: Upcoming changes to Confluence Cloud REST APIs to improve user privacy"
platform: cloud
product: confcloud
category: devguide
subcategory: updates
date: "2018-05-25"
---

# Major changes to Confluence Cloud REST APIs are coming to improve user privacy

Throughout 2018 and 2019, Atlassian will undertake a number of changes to our products and APIs
in order to improve user privacy in accordance with the [European General Data Protection
Regulation (GDPR)]. In addition to pursuing relevant certifications and data handling standards,
we will be rolling out changes to Atlassian Cloud product APIs to consolidate how personal data
about Atlassian product users is accessed by API consumers.

This page summarizes the relevant API changes that we expect to make in the future. Where
possible, we provide a link to specific Jira issues that you can track to stay up to date about
specific changes and when they will go into effect. We encourage you to watch these issues and
*check this page regularly* in order to stay up to date about any API changes.

## Introduction of Atlassian account ID

User objects are returned by a number of Confluence REST API endpoints. For example:

- The `/user/current` resource returns a representation of the current user
- The `/group/{groupName}/member` resource returns representations of each user in a group
- The `/content` resource can expand user-based properties like the creator of the page, version or child comments

For a full list of affected APIs, see the table at the bottom of this post.

{{% note %}}
In all cases where Confluence APIs return user details, the object body now includes the user's Atlassian account
ID (`accountId`). The `accountId` is a unique identifier for an Atlassian account user and should be considered
the primary key for interacting with users via Atlassian APIs.
{{% /note %}}

If you store user data, we strongly encourage you to use `accountId` to identify users.

## Changes to Confluence user objects

When a user object is returned by a Confluence API today, it includes a number of attributes
about a user, like `emailAddress`,`displayName` and `profilePicture`. These user objects will
change substantially following the deprecation period. Below is a summary of changes:

|Attribute|Status|
|---|---|
|`self`|Changed to reference Atlassian account API URL.|
|`username`|Changed to return a system generated value for new users without notice and then removed following the deprecation period.|
|`userKey`|Removed following the deprecation period.|
|`accountId`|Will always be returned. Primary identifier for users.|
|`email`|Will be returned if allowed by user's privacy settings. May be null.|
|`displayName`|Value returned is driven by user's privacy settings, will be non-null.|
|`profilePicture`|Current attribute will be removed following the deprecation period. New avatar resources will be introduced.|

Atlassian will provide a public **Atlassian account API** to access individual user details later this year.
Please watch [CONFCLOUD-59871] to be notified about the changes.

## Removal of username values from various Confluence API resources

Currently, Confluence users also have a `username` identifier, which is a mutable, per-user identifier within
a single Confluence instance. As we expect API consumers to use `accountId` as the primary identifier for
users, the `username` and `userKey` values will be removed from all locations in the future, including as
references for mentioning users in storage format, such as `<ri:user username="myusername"/>`. This will be
replaced with `<ri:user accountId="abc123"/>`. Please watch [CONFCLOUD-59872] to be notified about these changes.

## Updates to CQL fields that accept username, userkey or email as input

A number of fields in CQL accept `username`, `userKey` or `email` as input. For instance `creator.username`,
`user.email` or `contributor.userKey`. Fields that accept `username` or `userKey` will be removed, and will
instead accept `accountId`. Fields that accept email as part of the CQL query will only match a user where
the email address is visible for that users privacy settings. Please watch [CONFCLOUD-59873] to be notified
about these changes.

## Updates to Confluence APIs which accept user user name or key as input

A number of Confluence API endpoints currently accept Confluence usernames or userkeys as path parameters, query
parameters, or in request bodies. Confluence will introduce new versions for each affected API that accepts the
`username` or `userKey` parameters. In all cases, requests that previously used a `username` or `userKey` will
only accept an `accountId` in the new API version.

The tables below contain affected API resources and tickets to watch.

## Confluence REST APIs changing in response to GDPR

### Template

The user representation returned by these resources is changing as described above.

|Resource|Methods|Ticket to watch for updates|
|---|---|---|
|/rest/api/template|POST, PUT|[CONFCLOUD-59875]|
|/rest/api/template/blueprint?|GET|[CONFCLOUD-59875]|
|/rest/api/template/page?|GET|[CONFCLOUD-59875]|
|/rest/api/template/:contentTemplateId|GET|[CONFCLOUD-59875]|

### User

The `username` and `userkey` query params are no longer supported. Use the existing `accountId` query parameter
as the replacement. The user representation returned by these resources is changing as described above.

|Resource|Methods|Ticket to watch for updates|
|---|---|---|
|/rest/api/user?|GET|[CONFCLOUD-59874]|
|/rest/api/user/anonymous?|GET|[CONFCLOUD-59874]|
|/rest/api/user/current?|GET|[CONFCLOUD-59874]|
|/rest/api/user/memberof?|GET|[CONFCLOUD-59874]|
|/rest/api/user/watch/content/:contentId?|GET, POST, DELETE|[CONFCLOUD-59874]|
|/rest/api/user/watch/label/:labelName?|GET, POST, DELETE|[CONFCLOUD-59874]|
|/rest/api/user/watch/space/:spaceKey?|GET, POST, DELETE|[CONFCLOUD-59874]|

### Content

The user representation returned by these resources is changing as described above, including fields in the version,
history and restrictions expansions. Rendered macro output will only include user information that the requesting
user is allowed to see.

|Resource|Methods|Ticket to watch for updates|
|---|---|---|
|/rest/api/content?|GET, POST|[CONFCLOUD-59876]|
|/rest/api/content/blueprint/instance/:draftId?|POST, PUT|[CONFCLOUD-59876]|
|/rest/api/content/:id?|GET, PUT|[CONFCLOUD-59876]|
|/rest/api/content/:id/child?|GET|[CONFCLOUD-59876]|
|/rest/api/content/:id/child/attachment?|GET, POST, PUT|[CONFCLOUD-59876]|
|/rest/api/content/:id/child/comment?|GET|[CONFCLOUD-59876]|
|/rest/api/content/:id/child/:type?|GET|[CONFCLOUD-59876]|
|/rest/api/content/:id/descendant?|GET|[CONFCLOUD-59876]|
|/rest/api/content/:id/descendant/:type?|GET|[CONFCLOUD-59876]|
|/rest/api/content/:id/history?|GET|[CONFCLOUD-59876]|
|/rest/api/content/:id/history/:version/macro/id/:macroId|GET|[CONFCLOUD-59876]|
|/rest/api/content/:id/notification/child-created?|GET|[CONFCLOUD-59876]|
|/rest/api/content/:id/notification/created?|GET|[CONFCLOUD-59876]|
|/rest/api/content/:id/restriction?|GET, POST, PUT|[CONFCLOUD-59876]|
|/rest/api/content/:id/restriction/byOperation?|GET|[CONFCLOUD-59876]|
|/rest/api/content/:id/restriction/byOperation/<br/>:operationKey?|GET|[CONFCLOUD-59876]|
|/rest/api/content/:id/restriction/byOperation/<br/>:operationKey/user?|GET, POST, PUT|[CONFCLOUD-59876]|
|/rest/api/content/:id/version?|GET|[CONFCLOUD-59876]|
|/rest/api/content/:id/version/:versionNumber?|GET|[CONFCLOUD-59876]|
|/rest/api/contentbody/convert/:to?|GET|[CONFCLOUD-59876]|

### Search

The format of the CQL query param is changing when querying user fields, as well as the embedded user representations in content and spaces.

|Resource|Methods|Ticket to watch for updates|
|---|---|---|
|/rest/api/content/search?cql={{cql}}|GET|[CONFCLOUD-59876]|
|/rest/api/search?cql={{cql}}|GET|[CONFCLOUD-59877]|

### Groups

The user representation returned by this resource is changing as described above.

|Resource|Methods|Ticket to watch for updates|
|---|---|---|
|/rest/api/group/:groupName/member?|GET|[CONFCLOUD-59878]|

### Relations

The user representation returned by these resources is changing as described above.

|Resource|Methods|Ticket to watch for updates|
|---|---|---|
|/rest/api/relation/:relationName/from/<br/>:sourceType/:sourceKey/to/:targetType?|GET|[CONFCLOUD-59879]|
|/rest/api/relation/:relationName/from/<br/>:sourceType/:sourceKey/to/:targetType/:targetKey?|GET, PUT, DELETE|[CONFCLOUD-59879]|
|/rest/api/relation/:relationName/to/<br/>:targetType/:targetKey/from/:sourceType?|GET|[CONFCLOUD-59879]|

### Spaces

The user representation returned by these resources is changing as described above, including fields in the history
and permissions expansions.

|Resource|Methods|Ticket to watch for updates|
|---|---|---|
|/rest/api/space?|GET, POST|[CONFCLOUD-59880]|
|/rest/api/space/_private|POST|[CONFCLOUD-59880]|
|/rest/api/space/:spaceKey?|GET, PUT|[CONFCLOUD-59880]|
|/rest/api/space/:spaceKey/content?|GET|[CONFCLOUD-59880]|
|/rest/api/space/:spaceKey/content/:type?|GET|[CONFCLOUD-59880]|

### Other APIs changing in response to GDPR

|API|Resources|Ticket to watch for updates|
|---|---|---|
|**Webhooks**|creatorKey, creatorName, modifierKey, modifierName, user, userKey|[CONFCLOUD-59881]<br/>Will be removed and replaced with accountId equivalents.|
|**Context parameters**|user_id|[CONFCLOUD-59882]<br/>Already deprecated and will be removed.|
|**Context parameters**|user_key|[CONFCLOUD-59882]<br/>Already deprecated and will be removed.|
|User context JWT claim||See related notice.||

[European General Data Protection Regulation (GDPR)]: https://www.atlassian.com/blog/announcements/atlassian-and-gdpr-our-commitment-to-data-privacy
[Atlassian REST API Policy]: /platform/marketplace/atlassian-rest-api-policy/
[CONFCLOUD-59871]: https://jira.atlassian.com/browse/CONFCLOUD-59871
[CONFCLOUD-59872]: https://jira.atlassian.com/browse/CONFCLOUD-59872
[CONFCLOUD-59873]: https://jira.atlassian.com/browse/CONFCLOUD-59873
[CONFCLOUD-59874]: https://jira.atlassian.com/browse/CONFCLOUD-59874
[CONFCLOUD-59875]: https://jira.atlassian.com/browse/CONFCLOUD-59875
[CONFCLOUD-59876]: https://jira.atlassian.com/browse/CONFCLOUD-59876
[CONFCLOUD-59877]: https://jira.atlassian.com/browse/CONFCLOUD-59877
[CONFCLOUD-59878]: https://jira.atlassian.com/browse/CONFCLOUD-59878
[CONFCLOUD-59879]: https://jira.atlassian.com/browse/CONFCLOUD-59879
[CONFCLOUD-59880]: https://jira.atlassian.com/browse/CONFCLOUD-59880
[CONFCLOUD-59881]: https://jira.atlassian.com/browse/CONFCLOUD-59881
[CONFCLOUD-59882]: https://jira.atlassian.com/browse/CONFCLOUD-59882