---
title: "Authentication for apps"
platform: cloud
product: jiracloud
category: devguide
subcategory: security
aliases:
    - /cloud/jira/platform/authentication-for-add-ons.html
    - /cloud/jira/platform/authentication-for-add-ons.md
date: "2017-08-24"
---
{{< include path="docs/content/cloud/connect/concepts/authentication.snippet.md" >}}