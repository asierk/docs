---
title: "Adding a dropdown to an Agile board"
platform: cloud
product: jswcloud
category: devguide
subcategory: learning 
guides: tutorials
aliases:
- /jiracloud/tutorial-adding-a-dropdown-to-an-agile-board-39990287.html
- /jiracloud/tutorial-adding-a-dropdown-to-an-agile-board-39990287.md
confluence_id: 39990287
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39990287
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39990287
date: "2017-09-11"
---
# Adding a dropdown to an Agile board

This tutorial shows you how to add a new dropdown to a board in Jira Software, by using a Jira Software module in your Connect app. This dropdown will appear in the top right corner of an Agile board, next to the **Board** dropdown.

You'll use a Jira Software module to add this new dropdown. To do this, you'll create a new web-section at the location: `jira.agile.board.tools`, then add a web-item or web-panel inside the web-section.

*Screenshot: Example web-section with a web-panel*

![](../images/screenshot-2015-06-19-at-13.43.15.png)

{{% note title="About these instructions"%}}
You can use any supported combination of OS and IDE to create this app. These instructions were written using Sublime Text editor on Mac OS X. If you are using a different IDE and/or OS, you should use the equivalent operations for your specific environment.{{% /note %}}

## Before you begin

-   If you haven't built a Connect app before, read the [Getting started] tutorial. You'll learn the fundamentals of Connect development and build a basic Connect app that you can use with this tutorial.

## Instructions

#### Step 1. Create a web-section and web-panel in your `atlassian-connect.json`

1.  Edit the `atlassian-connect.json` file in your app project.
2.  Add the following code for the `web-section` to the `modules` context:

    ``` json
    "webSections": [
          {
            "key": "board-links",
            "location": "jira.agile.board.tools",
            "weight": 10,
            "name": {
              "value": "My Addon Dropdown"
            }
          }
        ],
    ```
3.  Add the following code for the `web-panel` to the `modules` context:
    ``` json
    "webPanels": [
          {
            "key": "my-web-panel",
            "url": "web-panel?id={board.id}&mode={board.screen}",
            "name": {
              "value": "My Web Panel"
            },
            "location": "board-links",
            "layout": {
              "width": "100px",
              "height": "100px"
            }
          }
        ]
    ```

#### Step 2. Add server side code to render the web panel

The server-side code required to render the web panel is shown below. This code is for <a href="https://bitbucket.org/atlassian/atlassian-connect-express" class="external-link">atlassian-connect-express</a>, the Node.js framework (with Express), but you can use a different technology stack if you wish.

1.  Navigate to the **routes** directory in your app project and edit the `index.js` file.
2.  Add the following code to the file and save it:

    ``` json
    app.get('/web-panel', function(req,res){
        res.render("dropdown-web-panel", {id : req.query['id'], mode : req.query['mode'] });
    });
    ```
    This adds the 'configuration' route to your app.
3.  Navigate to the **views** directory in your app project and create a new `dropdown-web-panel.hbs` file. 
4.  Add the following code to the `dropdown-web-panel.hbs` file and save it:
    ``` xml
    <!DOCTYPE html>
    <html>
      <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="ap-local-base-url" content="http://localhost:2990">
        <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.9.17/css/aui.min.css" media="all">
        <script src="https://ac-getting-started.atlassian.net/atlassian-connect/all.js"></script>
      </head>
      <body>
        <p id="custom-message">We are viewing board {{id}} in {{mode}} mode.</p>
      </body>
    </html>
    ```

#### Step 3. Check that it all works

1.  Start up and deploy your app. Run the following command on your Jira Cloud instance, if it's not running already:
    ``` bash
    npm start
    ```
2.  Navigate to the board for any Jira Software project. You should see a page like this:
    ![Alt text](../images/screenshot-2015-06-19-at-13.56.42.png)
3.  Click the dropdown you have added, and it should display your web panel.

{{% tip title="Congratulations!"%}}You've just added a dropdown to an Agile board in your Connect app.{{% /tip %}}

## Next steps

If you've finished this tutorial, check out these other [Jira Software tutorials].


  [Getting started]: /cloud/jira/software/getting-started
  [Jira Software tutorials]: /cloud/jira/software/tutorials-and-guides
