---
title: Scopes
platform: cloud
product: jsdcloud
category: devguide
subcategory: blocks
aliases:
- /cloud/jira/service-desk/scopes.html
- /cloud/jira/service-desk/scopes.md
date: "2017-08-25"
---
{{< include path="docs/content/cloud/connect/reference/jira-scopes.snippet.md">}}