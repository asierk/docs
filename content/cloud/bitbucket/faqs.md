---
title: "FAQs"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: learning
learning: guides 
date: "2017-02-27"
---

# Atlassian Connect FAQ

### What is Atlassian Connect?
Atlassian Connect is a distributed app technology for extending Atlassian applications such as
Bitbucket Cloud, Jira, and Confluence. Atlassian Connect is built for a world where software runs wherever,
whenever, and however. Atlassian Connect apps extend Atlassian applications entirely over standard
web protocols and APIs, such as HTTP and REST. This frees developers from traditional app platform
constraints, giving them new choices of programming language and deployment options. Regardless of
delivery model or location, Atlassian applications can be extended with Atlassian Connect apps,
so developers can be confident their apps can solve anyone's business problem.

### Where are the docs?

You've found them!

The primary documentation for Atlassian Connect [is here](/cloud/bitbucket). Individual tools that you
can use with Atlassian Connect, such as
[atlassian-connect-express](https://bitbucket.org/atlassian/atlassian-connect-express), may provide
additional documentation.

### What other information resources exist?
- [Bitbucket Cloud on the Atlassian Developer Community](http://go.atlassian.com/developer-community/c/bitbucket-development/bitbucket-cloud)
- [Bitbucket Cloud issue tracker](https://bitbucket.org/site/master/issues/)

### What products support Atlassian apps?

Atlassian apps can be written in:

- Bitbucket Cloud
- [Jira Platform Cloud](/cloud/jira/platform/integrating-with-jira-cloud/)
- [Jira Software Cloud](/cloud/jira/software/)
- [Jira ServiceDesk Cloud](/cloud/jira/service-desk/integrating-with-jsd-cloud/)
- [Confluence Cloud](/cloud/confluence/integrating-with-confluence-cloud/)

### How can I request new features for Atlassian Connect?

If there's a feature you'd like to see added to Atlassian Connect, such as a new module type or a
particular REST method, please let us know. Submit new feature requests, bugs, and feature votes in
the [Bitbucket Cloud issue tracker](https://bitbucket.org/site/master/issues/).

### How does Atlassian Connect work?

An Atlassian Connect app is simply a web application that describes itself with an Atlassian
app descriptor file. That descriptor includes authentication settings and declares the app's
capabilities. Capabilities take the form of modules. A module specifies an HTTP
resource exposed by the app and the place where that resource interacts with the Atlassian app.

### What languages, frameworks, & hosts will be supported?

Because your remote app is decoupled from the Atlassian application, using only HTTP and
REST to communicate, you are free to build in any language, use any framework, and deploy in any
manner you wish.

### Does Atlassian provide hosting for apps?

No. You may choose from the many great PaaS or hosting providers.

### How are Atlassian Connect apps supported by Atlassian?

Atlassian Connect apps receive the same level of support that traditional apps do today. Atlassian
supports the platform, the SDK and the documentation. Vendors are responsible for supporting the
apps they build and the customers who use those apps.

### How should app vendors support their customers?

Vendors must provide a support channel when listing on the Marketplace. That support channel should
be an issue tracker or ticketing system where a customer can file and track issues. An email address
is insufficient. We can provide a Jira instance for vendors who wish to use it for support and issue
tracking. Atlassian believes in a policy of transparency, and that information should be open by
default. As such, we encourage (but do not require) you to make your tracker open to the public.

In the future, there may be SLAs around support tickets for some or all vendors.

### What are the support requirements for app vendors relative to Atlassian?

If Atlassian files a support ticket in your system, we ask for next-business-day response time, and
resolution time as quickly as possible. We reserve the right to disable your app and remove it
from the Marketplace if problems cannot be resolved.

### What are the service requirements for an app?

There are currently no service-level agreements enforced for apps in the Atlassian Marketplace.
However, in cloud products, the service level is very important to customers. We intend to measure each
app's current status and uptime and make that information available to customers, similar to the
way that [cloud products do](https://www.atlassian.com/cloud/status). We encourage
app providers to strive for 99.9% uptime.

### What policies must apps observe about customer data?

As an Atlassian Connect developer, you must be responsible with the data entrusted to you by your
customers. Atlassian Connect developers must create and display a Data Security & Privacy Statement
and include that in your Marketplace Listing. Including simple and easily described information
about your service in your Data Security and Privacy Statement will reassure your customers that you
are acting as a professional and trustworthy provider of hosted software.

For reference, here are Atlassian's relevant policies:

* [Atlassian Cloud Product Security Statement](https://www.atlassian.com/hosted/security)
* [Storage Policy](https://confluence.atlassian.com/display/AODM/OnDemand+Storage+Policy)
* [Data Policy](https://confluence.atlassian.com/display/Cloud/About+Your+Data)
* [Atlassian's Privacy Policy](https://www.atlassian.com/company/privacy)
* [Atlassian's Security Advisory Publishing Policy](https://www.atlassian.com/security/security-advisory-pubpolicy)

Your policy may cover the following areas:

* **Data storage and location:** Explain where your application will store data from your customers
and where (physically) the data will be stored. It is your responsibility to comply with all local
laws.
* **Backups:** Explain your backup and recovery policy for customer data. You should publish your
[RTO](http://en.wikipedia.org/wiki/Recovery_time_objective) and [RPO](http://en.wikipedia.org/wiki/Recovery_point_objective)
targets, and explain if and when data is moved off site. For cloud products, backups are made daily, and stored
off site on a weekly basis.
* **Account removal and data retention:** Explain how a customer can close an account and completely
remove their data from your service. For Atlassian cloud applications, customer data is retained for 15 days
after account removal and then deleted (and unrecoverable) after that time.
* **Data portability:** Explain if and how a customer can extract their data from your service. For
example, is it possible to move from your hosted service to a downloaded version of your software?
* **Application and infrastructure security:** Explain what security measures you've taken in your
application and infrastructure, for example on-disk data encryption or encrypted data transfer between servers.
* **Security disclosure:** Explain how and under what circumstances you would notify customers about
security breaches or vulnerabilities. You should also indicate how a user or security researcher should
disclose a vulnerability found in your app to you. (Example from Atlassian:
[How to report a security issue](https://www.atlassian.com/trust/security/how-to-report-security-issue))
* **Privacy:** Explain that data collected during the use of your app will not be shared with
third parties except as required by law.

### What does this mean for a new app developer?

We expect that new developers, both commercial and internal, can start with Atlassian Connect.
They will use sandboxed UIs and remote APIs, which provide much more stability over time. If you are
integrating Atlassian tools with another service or remote application, this is the ideal path for
development.
